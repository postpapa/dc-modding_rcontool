@echo off
Set IP=ServerIP
Set RconPort=RconPort
Set AdminPW=AdminPassword
Set PollingRate=UpdateRateInSeconds
Set MCRconPath=PathToMCRCONexe


goto :init

:loop
    call :connection_check
    if %failed% NEQ false (
    	call :delay
    	goto loop
    	)
    call :output_log
    call :del_files
    call :delay
    goto loop

:connection_check
	mcrcon -c -H %IP% -P %RconPort% -p %AdminPW% "%RconCommand%" >> %IP%-%RconPort%-%RconCommand%.txt %dump%
	if ERRORLEVEL 1 (
		if %failed% NEQ true (
			call :output_log
			set dump=2^>nul
			set failed=true
			goto :eof
			) else (
				goto :eof
				)
	) else (
		set failed=false
		goto :eof
		)

:delay
	ping localhost -n %PollingRate% >nul
	goto :eof

:output_log
	findstr /v /r /c:"^$" /c:"^\ *$" "%IP%-%RconPort%-%RconCommand%.txt" > "%IP%-%RconPort%-%RconCommand%-second.txt"
    set /p response=<%IP%-%RconPort%-%RconCommand%-second.txt
    if "%response%" EQU "Authentication failed!" (
    	call :del_files
    	echo Pls Check Your Credentials!
    	pause
    	exit
    	)
    if "%response%" NEQ "Server received, But no response!! " (
        for /F "tokens=*" %%a in (%IP%-%RconPort%-%RconCommand%-second.txt) do echo %%a
        )
    goto :eof

:del_files
	del /f /q %IP%-%RconPort%-%RconCommand%.txt
    del /f /q %IP%-%RconPort%-%RconCommand%-second.txt
    goto :eof

:command
    mcrcon -c -H %IP% -P %RconPort% -p %AdminPW% "%command%"
    ping localhost -n 2 >nul
    goto main



:help
    echo.
    echo.
    echo        This is not a complete list of the available commands. You can use every Rcon Command you can find :) 
    echo.
    echo.
    echo ###################################################################################################################
    echo listplayers                          Shows a list of all players on the server with their SteamID
    echo broadcast MESSAGE                    Sends a "Message of the Day" styled message to the server
    echo serverchat                           Sends a yellow highlighted message into the chat
    echo serverchatto \"STEAM64ID\"           Sends a Message to Player ex.: serverchatto \"76561198006592627\" hello buddy
    echo setmessageofthe day MESSAGE          Set a new the Message of the Day
    echo showmessageoftheday                  Shows the MOTD to all Players
    echo kickplayer STEAM64ID                 Kicks the person from the server
    echo banplayer  STEAM64ID                 Bans the user from the server
    echo unbanplayer STEAM64ID                Unbans the user from the server
    echo renameplayer \"OLDNAME\" NEWNAME     Rename a Player ex.: renameplayer \"Penislord\" ChildSafe
    echo settimeofday TIMESTRING              Changes the time of day, for example cheat settimeofday 12:00
    echo saveworld                            Saves the current worldstate
    echo doexit                               Exits the current world, use after saving for a safe shutdown
    echo destroywilddinos                     Destroys all Wild Dinos on the map. They respawn.
    echo getgamelog                           Gets the ServerLog :=)
    echo getchat                              Gets the Chat :=)
    ping localhost -n 2 >nul
    goto main



:main
    echo ###################################################################################################################
    echo                           For an example list of commands pls enter:
    echo                                            help
    echo                          To receive the Log on a regular basis enter:
    echo                                           loop
    echo                       To receive only the chat on a regular basis enter:
    echo                                           ChatLoop
    echo ###################################################################################################################
    set /p command=Enter Rcon command:
    cls
    echo ###################################################################################################################
    if "%command%"=="Logloop" (
    	set RconCommand=getgamelog
    	goto loop
    	)
    if "%command%"=="ChatLoop" (
    	set RconCommand=getchat
    	goto loop
    	)
    if "%command%"=="help" (goto help) else (goto command)

:init
    Set failed=false
    Set dump=
    cd /D %MCRconPath%
    goto main