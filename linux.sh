#!/bin/bash

IP=ServerIP
RconPort=RconPort
AdminPW=AdminPassword
PollingRate=UpdateRate


no="Server received, But no response!! 
 "

start() {
    echo "+--------------------------------------------------------------------------------------------------------------------+"
    echo "|                                For an example list of commands pls enter:                                          |"
    echo "|                                                 help                                                               |"
    echo "|                               To receive the Log on a regular basis enter:                                         |"
    echo "|                                                LogLoop                                                             |"
    echo "|                            To receive only the chat on a regular basis enter:                                      |"
    echo "|                                                ChatLoop                                                            |"
    echo "+--------------------------------------------------------------------------------------------------------------------+"
    echo \| Enter Rcon command:
    read command
    clear
    echo "+--------------------------------------------------------------------------------------------------------------------+"
    if [ "$command" = "LogLoop" ]
        then logloop
    elif [ "$command" = "ChatLoop" ]
        then chatloop
    elif [ "$command" = "help" ]
        then help 
    else command
    fi
}

logloop() {
    mcrcon -c -H $IP -P $RconPort -p $AdminPW "getgamelog" > somefile.txt
    response=$(cat somefile.txt)
    if [ "$response" != "$no" ]
        then while IFS= read -r line; do echo $line; done < somefile.txt
    fi  
    sleep $PollingRate
    logloop
}

chatloop() {
    mcrcon -c -H $IP -P $RconPort -p $AdminPW "getchat" > somefile.txt
    response=$(cat somefile.txt)
    if [ "$response" != "$no" ]
        then while IFS= read -r line; do echo $line; done < somefile.txt
    fi  
    sleep $PollingRate
    chatloop
}

command() {
    mcrcon -c -H $IP -P $RconPort -p $AdminPW "$command"
    sleep 3
    start
}

help() {
    echo "|                                                                                                                    |"
    echo "|         This is not a complete list of available commands. You can use every Rcon command you can find :)          |"
    echo "|                                                                                                                    |"
    echo "|                                                                                                                    |" 
    echo "| listplayers                          Shows a list of all players on the server with their SteamID                  |"
    echo "| broadcast MESSAGE                    Sends a \"Message of the Day\" styled message to the server                     |"
    echo "| serverchat                           Sends a yellow highlighted message into the chat                              |"
    echo "| serverchatto \\\"STEAM64ID\\\"           Sends a Message to Player ex.: serverchatto \\\"76561198006592627\\\" hello buddy |"
    echo "| setmessageofthe day MESSAGE          Set a new the Message of the Day                                              |"
    echo "| showmessageoftheday                  Shows the MOTD to all Players                                                 |"
    echo "| kickplayer STEAM64ID                 Kicks the person from the server                                              |"
    echo "| banplayer  STEAM64ID                 Bans the user from the server                                                 |"
    echo "| unbanplayer STEAM64ID                Unbans the user from the server                                               |"
    echo "| renameplayer \\\"OLDNAME\\\" NEWNAME     Rename a Player ex.: renameplayer \\\"Penislord\\\" ChildSafe                     |"
    echo "| settimeofday TIMESTRING              Changes the time of day, for example cheat settimeofday 12:00                 |"
    echo "| saveworld                            Saves the current worldstate                                                  |"
    echo "| doexit                               Exits the current world, use after saving for a safe shutdown                 |"
    echo "| destroywilddinos                     Destroys all Wild Dinos on the map. They respawn.                             |"
    echo "| getgamelog                           Gets the ServerLog :=)                                                        |"
    echo "| getchat                              Gets the Chat :=)                                                             |"
    echo "+--------------------------------------------------------------------------------------------------------------------+"
    sleep 2
    start
}

start